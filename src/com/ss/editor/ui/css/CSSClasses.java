package com.ss.editor.ui.css;

/**
 * The list of custom css classes.
 *
 * @author JavaSaBr.
 */
public interface CSSClasses {

    /**
     * The constant MAIN_FONT_11.
     */
    String MAIN_FONT_11 = "main-font-11";
    /**
     * The constant MAIN_FONT_12.
     */
    String MAIN_FONT_12 = "main-font-12";
    /**
     * The constant MAIN_FONT_13.
     */
    String MAIN_FONT_13 = "main-font-13";
    /**
     * The constant MAIN_FONT_14.
     */
    String MAIN_FONT_14 = "main-font-14";
    /**
     * The constant MAIN_FONT_14_BOLD.
     */
    String MAIN_FONT_14_BOLD = "main-font-14-bold";
    /**
     * The constant MAIN_FONT_15.
     */
    String MAIN_FONT_15 = "main-font-15";
    /**
     * The constant MAIN_FONT_15_BOLD.
     */
    String MAIN_FONT_15_BOLD = "main-font-15-bold";
    /**
     * The constant MAIN_FONT_15_BOLD_WITH_SHADOW.
     */
    String MAIN_FONT_15_BOLD_WITH_SHADOW = "main-font-15-bold-with-shadow";
    /**
     * The constant MAIN_FONT_17.
     */
    String MAIN_FONT_17 = "main-font-17";
    /**
     * The constant MAIN_FONT_20.
     */
    String MAIN_FONT_20 = "main-font-20";

    /**
     * The constant SPECIAL_FONT_12.
     */
    String SPECIAL_FONT_12 = "special-font-12";
    /**
     * The constant SPECIAL_FONT_13.
     */
    String SPECIAL_FONT_13 = "special-font-13";
    /**
     * The constant SPECIAL_FONT_14.
     */
    String SPECIAL_FONT_14 = "special-font-14";
    /**
     * The constant SPECIAL_FONT_15.
     */
    String SPECIAL_FONT_15 = "special-font-15";
    /**
     * The constant SPECIAL_FONT_16.
     */
    String SPECIAL_FONT_16 = "special-font-16";

    /**
     * The constant MONO_FONT_13.
     */
    String MONO_FONT_13 = "mono-font-13";

    /**
     * The constant DIALOG_ROOT.
     */
    String DIALOG_ROOT = "dialog-root";

    /**
     * The constant DIALOG_ACTIONS_ROOT.
     */
    String DIALOG_ACTIONS_ROOT = "dialog-actions-root";

    /**
     * The constant DIALOG_CONTENT_ROOT.
     */
    String DIALOG_CONTENT_ROOT = "dialog-content-root";

    /**
     * The constant EDITOR_BAR_BUTTON.
     */
    String EDITOR_BAR_BUTTON = "editor-bar-button";

    /**
     * The constant TRANSPARENT_TREE_VIEW.
     */
    String TRANSPARENT_TREE_VIEW = "transparent-tree-view";
    /**
     * The constant TRANSPARENT_TREE_CELL.
     */
    String TRANSPARENT_TREE_CELL = "transparent-tree-cell";
    /**
     * The constant TRANSPARENT_LIST_VIEW.
     */
    String TRANSPARENT_LIST_VIEW = "transparent-list-view";
    /**
     * The constant TRANSPARENT_LIST_CELL.
     */
    String TRANSPARENT_LIST_CELL = "transparent-list-cell";

    /**
     * The constant LIST_CELL_WITHOUT_PADDING.
     */
    String LIST_CELL_WITHOUT_PADDING = "list-cell-without-padding";

    /**
     * The constant LIST_VIEW_WITHOUT_SCROLL.
     */
    String LIST_VIEW_WITHOUT_SCROLL = "list-view-without-scroll";

    /**
     * The constant TRANSPARENT_MENU_ITEM.
     */
    String TRANSPARENT_MENU_ITEM = "transparent-menu-item";

    /**
     * The constant TRANSPARENT_COMBO_BOX.
     */
    String TRANSPARENT_COMBO_BOX = "transparent-combo-box";

    /**
     * The constant TOOLBAR_BUTTON.
     */
    String TOOLBAR_BUTTON = "toolbar-button";
    /**
     * The constant DRAWER_BUTTON.
     */
    String DRAWER_BUTTON = "drawer-button";

    /**
     * The constant FILE_EDITOR_TOOLBAR_BUTTON.
     */
    String FILE_EDITOR_TOOLBAR_BUTTON = "file-editor-toolbar-button";

    /**
     * The constant EDITING_TOGGLE_BUTTON_BIG.
     */
    String EDITING_TOGGLE_BUTTON_BIG = "editing-toggle-button-big";

    /**
     * The constant MATERIAL_PARAM_CONTROL.
     */
    String MATERIAL_PARAM_CONTROL = "material-param-control";
    /**
     * The constant ABSTRACT_PARAM_CONTROL.
     */
    String ABSTRACT_PARAM_CONTROL = "abstract-param-control";
}