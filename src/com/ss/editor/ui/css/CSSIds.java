package com.ss.editor.ui.css;

/**
 * The list of css ids which used in the editor.
 *
 * @author JavaSaBr
 */
public interface CSSIds {

    /**
     * The constant ROOT.
     */
    String ROOT = "Root";
    /**
     * The constant ROOT_CONTAINER.
     */
    String ROOT_CONTAINER = "RootContainer";

    /**
     * The constant EDITOR_BAR_COMPONENT.
     */
    String EDITOR_BAR_COMPONENT = "EditorBarComponent";

    /**
     * The constant EDITOR_LOADING_LAYER.
     */
    String EDITOR_LOADING_LAYER = "EditorLoadingLayer";
    /**
     * The constant EDITOR_LOADING_PROGRESS.
     */
    String EDITOR_LOADING_PROGRESS = "EditorLoadingProgress";

    /**
     * The constant MAIN_SPLIT_PANEL.
     */
    String MAIN_SPLIT_PANEL = "MainSplitPane";

    /**
     * The constant ASSET_COMPONENT.
     */
    String ASSET_COMPONENT = "AssetComponent";
    /**
     * The constant ASSET_COMPONENT_BAR.
     */
    String ASSET_COMPONENT_BAR = "AssetComponentBar";
    /**
     * The constant ASSET_COMPONENT_BAR_BUTTON.
     */
    String ASSET_COMPONENT_BAR_BUTTON = "AssetComponentBarButton";
    /**
     * The constant ASSET_COMPONENT_RESOURCE_TREE_CELL.
     */
    String ASSET_COMPONENT_RESOURCE_TREE_CELL = "AssetComponentResourceTreeCell";

    /**
     * The constant JME_PREVIEW_MANAGER_IMAGE_VIEW.
     */
    String JME_PREVIEW_MANAGER_IMAGE_VIEW = "JmePreviewManagerImageView";

    /**
     * The constant GLOBAL_LEFT_TOOL_COMPONENT.
     */
    String GLOBAL_LEFT_TOOL_COMPONENT = "GlobalLeftToolComponent";
    /**
     * The constant GLOBAL_BOTTOM_TOOL_COMPONENT.
     */
    String GLOBAL_BOTTOM_TOOL_COMPONENT = "GlobalBottomToolComponent";

    /**
     * The constant EDITOR_AREA_COMPONENT.
     */
    String EDITOR_AREA_COMPONENT = "EditorAreaComponent";

    /**
     * The constant FILE_EDITOR_TOOLBAR.
     */
    String FILE_EDITOR_TOOLBAR = "FileEditorToolbar";
    /**
     * The constant FILE_EDITOR_EDITOR_AREA.
     */
    String FILE_EDITOR_EDITOR_AREA = "FileEditorEditorArea";
    /**
     * The constant FILE_EDITOR_MAIN_SPLIT_PANE.
     */
    String FILE_EDITOR_MAIN_SPLIT_PANE = "FileEditorMainSplitPane";
    /**
     * The constant FILE_EDITOR_TOOL_SPLIT_PANE.
     */
    String FILE_EDITOR_TOOL_SPLIT_PANE = "FileEditorToolSplitPane";
    /**
     * The constant FILE_EDITOR_TOOL_COMPONENT.
     */
    String FILE_EDITOR_TOOL_COMPONENT = "FileEditorToolComponent";

    /**
     * The constant TEXT_EDITOR_TEXT_AREA.
     */
    String TEXT_EDITOR_TEXT_AREA = "TextEditorTextArea";

    /**
     * The constant EDITOR_DIALOG_BACKGROUND.
     */
    String EDITOR_DIALOG_BACKGROUND = "EditorDialogBackground";
    /**
     * The constant EDITOR_DIALOG_HEADER.
     */
    String EDITOR_DIALOG_HEADER = "EditorDialogHeader";
    /**
     * The constant EDITOR_DIALOG_HEADER_BUTTON_CLOSE.
     */
    String EDITOR_DIALOG_HEADER_BUTTON_CLOSE = "EditorDialogHeaderButtonClose";
    /**
     * The constant EDITOR_DIALOG_BUTTON_OK.
     */
    String EDITOR_DIALOG_BUTTON_OK = "EditorDialogButtonOk";
    /**
     * The constant EDITOR_DIALOG_BUTTON_CANCEL.
     */
    String EDITOR_DIALOG_BUTTON_CANCEL = "EditorDialogButtonCancel";
    /**
     * The constant EDITOR_DIALOG_LABEL_WARNING.
     */
    String EDITOR_DIALOG_LABEL_WARNING = "EditorDialogLabelWarning";
    /**
     * The constant EDITOR_DIALOG_SHORT_LABEL.
     */
    String EDITOR_DIALOG_SHORT_LABEL = "EditorDialogShortLabel";
    /**
     * The constant EDITOR_DIALOG_DYNAMIC_LABEL.
     */
    String EDITOR_DIALOG_DYNAMIC_LABEL = "EditorDialogDynamicLabel";
    /**
     * The constant EDITOR_DIALOG_FIELD.
     */
    String EDITOR_DIALOG_FIELD = "EditorDialogField";

    /**
     * The constant ASSET_EDITOR_DIALOG_BUTTON_CONTAINER.
     */
    String ASSET_EDITOR_DIALOG_BUTTON_CONTAINER = "AssetEditorDialogButtonContainer";
    /**
     * The constant ASSET_EDITOR_DIALOG_RESOURCES_CONTAINER.
     */
    String ASSET_EDITOR_DIALOG_RESOURCES_CONTAINER = "AssetEditorDialogResourcesContainer";
    /**
     * The constant ASSET_EDITOR_DIALOG_PREVIEW_CONTAINER.
     */
    String ASSET_EDITOR_DIALOG_PREVIEW_CONTAINER = "AssetEditorDialogPreviewContainer";

    /**
     * The constant PARTICLES_ASSET_EDITOR_DIALOG_SETTINGS_CONTAINER.
     */
    String PARTICLES_ASSET_EDITOR_DIALOG_SETTINGS_CONTAINER = "ParticlesAssetEditorDialogSettingsContainer";
    /**
     * The constant PARTICLES_ASSET_EDITOR_DIALOG_PREVIEW_CONTAINER.
     */
    String PARTICLES_ASSET_EDITOR_DIALOG_PREVIEW_CONTAINER = "ParticlesAssetEditorDialogPreviewContainer";
    /**
     * The constant PARTICLES_ASSET_EDITOR_DIALOG_LABEL.
     */
    String PARTICLES_ASSET_EDITOR_DIALOG_LABEL = "ParticlesAssetEditorDialogLabel";
    /**
     * The constant PARTICLES_ASSET_EDITOR_DIALOG_CONTROL.
     */
    String PARTICLES_ASSET_EDITOR_DIALOG_CONTROL = "ParticlesAssetEditorDialogControl";

    /**
     * The constant MATERIAL_FILE_EDITOR_TOOLBAR_LABEL.
     */
    String MATERIAL_FILE_EDITOR_TOOLBAR_LABEL = "MaterialFileEditorToolbarLabel";
    /**
     * The constant MATERIAL_FILE_EDITOR_TOOLBAR_BOX.
     */
    String MATERIAL_FILE_EDITOR_TOOLBAR_BOX = "MaterialFileEditorToolbarBox";
    /**
     * The constant MATERIAL_FILE_EDITOR_TOOLBAR_SMALL_BOX.
     */
    String MATERIAL_FILE_EDITOR_TOOLBAR_SMALL_BOX = "MaterialFileEditorToolbarSmallBox";
    /**
     * The constant MATERIAL_FILE_EDITOR_PROPERTIES_COMPONENT.
     */
    String MATERIAL_FILE_EDITOR_PROPERTIES_COMPONENT = "MaterialFileEditorPropertiesComponent";

    /**
     * The constant MATERIAL_PARAM_CONTROL_PARAM_NAME.
     */
    String MATERIAL_PARAM_CONTROL_PARAM_NAME = "MaterialParamControlParamName";
    /**
     * The constant MATERIAL_PARAM_CONTROL_COLOR_PICKER.
     */
    String MATERIAL_PARAM_CONTROL_COLOR_PICKER = "MaterialParamColorPicker";
    /**
     * The constant MATERIAL_PARAM_CONTROL_COMBO_BOX.
     */
    String MATERIAL_PARAM_CONTROL_COMBO_BOX = "MaterialParamComboBox";
    /**
     * The constant MATERIAL_PARAM_CONTROL_BUTTON.
     */
    String MATERIAL_PARAM_CONTROL_BUTTON = "MaterialParamControlButton";
    /**
     * The constant MATERIAL_PARAM_CONTROL_CHECKBOX.
     */
    String MATERIAL_PARAM_CONTROL_CHECKBOX = "MaterialParamControlCheckbox";
    /**
     * The constant MATERIAL_PARAM_CONTROL_SPINNER.
     */
    String MATERIAL_PARAM_CONTROL_SPINNER = "MaterialParamControlSpinner";

    /**
     * The constant TEXTURE_2D_MATERIAL_PARAM_CONTROL_PREVIEW.
     */
    String TEXTURE_2D_MATERIAL_PARAM_CONTROL_PREVIEW = "Texture2DMaterialParamControlPreview";

    /**
     * The constant MATERIAL_RENDER_STATE_POLY_OFFSET_FIELD.
     */
    String MATERIAL_RENDER_STATE_POLY_OFFSET_FIELD = "MaterialRenderStatePolyOffsetField";

    /**
     * The constant SETTINGS_DIALOG_MESSAGE_LABEL.
     */
    String SETTINGS_DIALOG_MESSAGE_LABEL = "SettingsDialogMessageLabel";
    /**
     * The constant SETTINGS_DIALOG_TAB_PANE.
     */
    String SETTINGS_DIALOG_TAB_PANE = "SettingsDialogTabPane";
    /**
     * The constant SETTINGS_DIALOG_LABEL.
     */
    String SETTINGS_DIALOG_LABEL = "SettingsDialogLabel";
    /**
     * The constant SETTINGS_DIALOG_FIELD.
     */
    String SETTINGS_DIALOG_FIELD = "SettingsDialogField";

    /**
     * The constant MODEL_NODE_TREE_CELL.
     */
    String MODEL_NODE_TREE_CELL = "ModelNodeTreeCell";
    /**
     * The constant MODEL_NODE_TREE_CELL_DRAGGED.
     */
    String MODEL_NODE_TREE_CELL_DRAGGED = "ModelNodeTreeCellDragged";
    /**
     * The constant MODEL_NODE_TREE_CELL_DROP_AVAILABLE.
     */
    String MODEL_NODE_TREE_CELL_DROP_AVAILABLE = "ModelNodeTreeCellDropAvailable";

    /**
     * The constant MODEL_PARAM_CONTROL_VECTOR4F_FIELD.
     */
    String MODEL_PARAM_CONTROL_VECTOR4F_FIELD = "ModelParamControlVector4fField";
    /**
     * The constant MODEL_PARAM_CONTROL_INFLUENCER_ELEMENT.
     */
    String MODEL_PARAM_CONTROL_INFLUENCER_ELEMENT = "ModelParamControlInfluencerElement";
    /**
     * The constant MODEL_PARAM_CONTROL_INFLUENCER_CONTROL.
     */
    String MODEL_PARAM_CONTROL_INFLUENCER_CONTROL = "ModelParamControlInfluencerControl";
    /**
     * The constant MODEL_PARAM_CONTROL_INFLUENCER_ICON_BUTTON.
     */
    String MODEL_PARAM_CONTROL_INFLUENCER_ICON_BUTTON = "ModelParamControlInfluencerIconButton";

    /**
     * The constant IMAGE_CHANNEL_PREVIEW.
     */
    String IMAGE_CHANNEL_PREVIEW = "ImageChannelPreview";
    /**
     * The constant IMAGE_CHANNEL_PREVIEW_IMAGE_CONTAINER.
     */
    String IMAGE_CHANNEL_PREVIEW_IMAGE_CONTAINER = "ImageChannelPreviewImageContainer";

    /**
     * The constant CHOOSE_RESOURCE_CONTROL_BUTTON.
     */
    String CHOOSE_RESOURCE_CONTROL_BUTTON = "ChooseResourceControlButton";

    /**
     * The constant CHOOSE_TEXTURE_CONTROL_TEXTURE_LABEL.
     */
    String CHOOSE_TEXTURE_CONTROL_TEXTURE_LABEL = "ChooseTextureControlTextureLabel";
    /**
     * The constant CHOOSE_TEXTURE_CONTROL_PREVIEW.
     */
    String CHOOSE_TEXTURE_CONTROL_PREVIEW = "ChooseTextureControlPreview";

    /**
     * The constant CHOOSE_FOLDER_CONTROL_FOLDER_LABEL.
     */
    String CHOOSE_FOLDER_CONTROL_FOLDER_LABEL = "ChooseFolderControlFolderLabel";

    /**
     * The constant AUDIO_VIEWER_EDITOR_BUTTON_CONTAINER.
     */
    String AUDIO_VIEWER_EDITOR_BUTTON_CONTAINER = "AudioViewerEditorButtonContainer";
    /**
     * The constant AUDIO_VIEWER_EDITOR_BUTTON.
     */
    String AUDIO_VIEWER_EDITOR_BUTTON = "AudioViewerEditorButton";
    /**
     * The constant AUDIO_VIEWER_EDITOR_PARAM_CONTAINER.
     */
    String AUDIO_VIEWER_EDITOR_PARAM_CONTAINER = "AudioViewerEditorParamContainer";
    /**
     * The constant AUDIO_VIEWER_EDITOR_PARAM_LABEL.
     */
    String AUDIO_VIEWER_EDITOR_PARAM_LABEL = "AudioViewerEditorParamLabel";
    /**
     * The constant AUDIO_VIEWER_EDITOR_PARAM_VALUE.
     */
    String AUDIO_VIEWER_EDITOR_PARAM_VALUE = "AudioViewerEditorParamValue";

    /**
     * The constant GENERATE_LOD_DIALOG_LIST_VIEW.
     */
    String GENERATE_LOD_DIALOG_LIST_VIEW = "GenerateLoDDialogListView";
    /**
     * The constant GENERATE_LOD_DIALOG_LIST_VIEW_CELL.
     */
    String GENERATE_LOD_DIALOG_LIST_VIEW_CELL = "GenerateLoDDialogListViewCell";

    /**
     * The constant LOG_VIEW.
     */
    String LOG_VIEW = "LogView";

    /**
     * The constant SCENE_APP_STATE_CONTAINER.
     */
    String SCENE_APP_STATE_CONTAINER = "SceneAppStateContainer";
    /**
     * The constant SCENE_APP_STATE_LIST_CELL.
     */
    String SCENE_APP_STATE_LIST_CELL = "SceneAppStateListCell";

    /**
     * The constant ABSTRACT_PARAM_CONTROL_CONTAINER.
     */
    String ABSTRACT_PARAM_CONTROL_CONTAINER = "AbstractParamControlContainer";
    /**
     * The constant ABSTRACT_PARAM_CONTROL_SPLIT_LINE.
     */
    String ABSTRACT_PARAM_CONTROL_SPLIT_LINE = "AbstractParamControlSplitLine";
    /**
     * The constant ABSTRACT_PARAM_CONTROL_PARAM_NAME_SINGLE_ROW.
     */
    String ABSTRACT_PARAM_CONTROL_PARAM_NAME_SINGLE_ROW = "AbstractParamControlParamNameSingleRow";
    /**
     * The constant ABSTRACT_PARAM_CONTROL_PARAM_NAME.
     */
    String ABSTRACT_PARAM_CONTROL_PARAM_NAME = "AbstractParamControlParamName";
    /**
     * The constant ABSTRACT_PARAM_CONTROL_CHECK_BOX.
     */
    String ABSTRACT_PARAM_CONTROL_CHECK_BOX = "AbstractParamControlCheckbox";
    /**
     * The constant ABSTRACT_PARAM_CONTROL_COLOR_PICKER.
     */
    String ABSTRACT_PARAM_CONTROL_COLOR_PICKER = "AbstractParamControlColorPicker";
    /**
     * The constant ABSTRACT_PARAM_CONTROL_LABEL_VALUE.
     */
    String ABSTRACT_PARAM_CONTROL_LABEL_VALUE = "AbstractParamControlLabelValue";
    /**
     * The constant ABSTRACT_PARAM_CONTROL_COMBO_BOX.
     */
    String ABSTRACT_PARAM_CONTROL_COMBO_BOX = "AbstractParamControlComboBox";
    /**
     * The constant ABSTRACT_PARAM_CONTROL_NUMBER_LABEL.
     */
    String ABSTRACT_PARAM_CONTROL_NUMBER_LABEL = "AbstractParamControlNumberLabel";
    /**
     * The constant ABSTRACT_PARAM_CONTROL_VECTOR3F_FIELD.
     */
    String ABSTRACT_PARAM_CONTROL_VECTOR3F_FIELD = "AbstractParamControlVector3fField";
    /**
     * The constant ABSTRACT_PARAM_CONTROL_NUMBER_LABEL2F.
     */
    String ABSTRACT_PARAM_CONTROL_NUMBER_LABEL2F = "AbstractParamControlNumberLabel2F";
    /**
     * The constant ABSTRACT_PARAM_CONTROL_VECTOR2F_FIELD.
     */
    String ABSTRACT_PARAM_CONTROL_VECTOR2F_FIELD = "AbstractParamControlVector2fField";
    /**
     * The constant ABSTRACT_PARAM_CONTROL_ELEMENT_LABEL.
     */
    String ABSTRACT_PARAM_CONTROL_ELEMENT_LABEL = "AbstractParamControlElementLabel";
    /**
     * The constant ABSTRACT_PARAM_CONTROL_ELEMENT_BUTTON.
     */
    String ABSTRACT_PARAM_CONTROL_ELEMENT_BUTTON = "AbstractParamControlElementButton";
    /**
     * The constant ABSTRACT_PARAM_CONTROL_MULTI_VALUES_CONTAINER.
     */
    String ABSTRACT_PARAM_CONTROL_MULTI_VALUES_CONTAINER = "AbstractParamControlMultiValuesContainer";

    /**
     * The constant IMAGE_VIEW_EDITOR_CONTAINER.
     */
    String IMAGE_VIEW_EDITOR_CONTAINER = "ImageViewEditorContainer";
    /**
     * The constant AUDIO_VIEW_EDITOR_CONTAINER.
     */
    String AUDIO_VIEW_EDITOR_CONTAINER = "AudioViewEditorContainer";

    /**
     * The constant ABSTRACT_NODE_TREE_CONTAINER.
     */
    String ABSTRACT_NODE_TREE_CONTAINER = "AbstractNodeTreeContainer";
    /**
     * The constant ABSTRACT_NODE_TREE_TRANSPARENT_CONTAINER.
     */
    String ABSTRACT_NODE_TREE_TRANSPARENT_CONTAINER = "AbstractNodeTreeTransparentContainer";

    /**
     * The constant ABSTRACT_DIALOG_GRID_SETTINGS_CONTAINER.
     */
    String ABSTRACT_DIALOG_GRID_SETTINGS_CONTAINER = "AbstractDialogGridSettingsContainer";

    /**
     * The constant PROCESSING_COMPONENT_CONTAINER.
     */
    String PROCESSING_COMPONENT_CONTAINER = "ProcessingComponentContainer";

    /**
     * The constant TERRAIN_EDITING_CONTROL_SETTINGS.
     */
    String TERRAIN_EDITING_CONTROL_SETTINGS = "TerrainEditingTextureControlSettings";
    /**
     * The constant TERRAIN_EDITING_TEXTURE_LAYER_SETTINGS.
     */
    String TERRAIN_EDITING_TEXTURE_LAYER_SETTINGS = "TerrainEditingTextureLayerSettings";
    /**
     * The constant TERRAIN_EDITING_TEXTURE_LAYERS_SETTINGS.
     */
    String TERRAIN_EDITING_TEXTURE_LAYERS_SETTINGS = "TerrainEditingTextureLayersSettings";
    /**
     * The constant TERRAIN_EDITING_TEXTURE_LAYERS_SETTINGS_BUTTONS.
     */
    String TERRAIN_EDITING_TEXTURE_LAYERS_SETTINGS_BUTTONS = "TerrainEditingTextureLayersSettingsButtons";

    /**
     * The constant FILE_CREATOR_DIALOG_CONTAINER.
     */
    String FILE_CREATOR_DIALOG_CONTAINER = "FileCreatorDialogContainer";
    /**
     * The constant FILE_CREATOR_DIALOG_GRID_SETTINGS_CONTAINER.
     */
    String FILE_CREATOR_DIALOG_GRID_SETTINGS_CONTAINER = "FileCreatorDialogGridSettingsContainer";

    /**
     * The constant STATS_APP_STATE_STATS_CONTAINER.
     */
    String STATS_APP_STATE_STATS_CONTAINER = "StatsAppStateStatsContainer";
    /**
     * The constant SCENE_EDITOR_STATS_CONTAINER.
     */
    String SCENE_EDITOR_STATS_CONTAINER = "SceneEditorStatsContainer";

    /**
     * The constant EDITOR_SCRIPTING_COMPONENT.
     */
    String EDITOR_SCRIPTING_COMPONENT = "EditorScriptingComponent";
    /**
     * The constant EDITOR_SCRIPTING_COMPONENT_BUTTON_RUN.
     */
    String EDITOR_SCRIPTING_COMPONENT_BUTTON_RUN = "EditorScriptingComponentButtonRun";
    /**
     * The constant GROOVY_EDITOR_COMPONENT.
     */
    String GROOVY_EDITOR_COMPONENT = "GroovyEditorComponent";
}
