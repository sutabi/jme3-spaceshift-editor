package com.ss.editor.ui;

/**
 * The interface Fx constants.
 *
 * @author JavaSaBr
 */
public interface FXConstants {

    /**
     * The constant CELL_SIZE.
     */
    int CELL_SIZE = 22;
}
